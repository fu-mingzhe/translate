# translate

## 介绍
简易的翻译软件

## 软件架构
dists 为windows的可执行文件
img 为资源文件
翻译小程序.py 为主文件

## 安装教程

### 1.安装Python环境
### [具体见https://gitee.com/fu-mingzhe/flask-website/blob/master/README.md](https://gitee.com/fu-mingzhe/flask-website/blob/master/README.md)
### 2.安装相关库
### 在项目文件目录下在终端运行如下命令

```
pip install -r requirements.txt
```


## 使用说明

### 运行翻译小程序.py
### Windows系统可运行dists文件下的文件

## 参与贡献

# 如果有什么问题或建议可以发邮件给2372769798@qq.com
# 我的gitee主页[https://gitee.com/fu-mingzhe](https://gitee.com/fu-mingzhe)
# 我的github主页[https://github.com/fu-mingzhe](https://github.com/fu-mingzhe)
